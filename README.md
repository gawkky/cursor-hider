A [FoundryVTT](http://foundryvtt.com/) module to give the users the ability to hide their cursors on the tabletop individually, instead of only being able to hide all or none.

Useful for example if you still want cursors in general, but you're sometimes messing with off-screen enemies as a Gamemaster and don't want your players to see.

See the [Features section](#features) for more info.

# Installation

1. Go to Foundry's Setup screen
1. Go to the "Add-On Modules" tab
1. Press "Install Module"
1. Recommended:
    1. Search "Cursor" at the top
    1. Press the "Install" button in the list
* Alternative:
    1. Paste `https://gitlab.com/foundry-azzurite/cursor-hider/-/jobs/artifacts/master/raw/dist/cursor-hider/module.json?job=build` into the text field at the bottom
    1. Press "Install" to the right of the text field

# Features

Hide your cursor and your ruler by clicking on the icon in the players list, or with the press of a button (default: Alt + C).

![demonstration](doc/cursor-hider.mp4)

You can configure this key, allow the hiding of the cursor only for certain permissions and hide your cursor by default:

![settings](doc/settings.png) 

## Module/Macro API

Very rudimentary. You can hide/show/toggle your own cursor by calling either `Azzu.CursorHider.hideCursor()`, `Azzu.CursorHider.showCursor()` or `Azzu.CursorHider.toggleCursor()`. 

For module authors, you can also use something along the lines of `import {hideCursor, showCursor, toggleCursor} from '../cursor-hider/cursor-hider.js';`

# Browser Compatibility

Only Firefox & Chrome are supported. I will not add support for any other browsers. Switch to Firefox if you are still using another browser.
